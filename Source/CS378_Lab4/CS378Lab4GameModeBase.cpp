// Fill out your copyright notice in the Description page of Project Settings.


#include "CS378Lab4GameModeBase.h"
#include "Lab4Character.h"
#include "Lab4PlayerController.h"

ACS378Lab4GameModeBase::ACS378Lab4GameModeBase()
{
	PlayerControllerClass = ALab4PlayerController::StaticClass();
	
	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Class'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'"));

	if (pawnBPClass.Object) {
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		DefaultPawnClass = ALab4Character::StaticClass();
	}
	
}


