// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378Lab4GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB4_API ACS378Lab4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACS378Lab4GameModeBase();
	
};
